//
//  MallCollectionViewDataSource.m
//  DCMall
//
//  Created by 吴恺 on 15/8/31.
//  Copyright (c) 2015年 wukai. All rights reserved.
//

#import "MallCollectionViewDataSource.h"

@interface MallCollectionViewDataSource ()
@property(copy, nonatomic, readwrite)
    MallCollectionViewCellConfigureBlock block;
@property(copy, nonatomic, readwrite) NSArray *dataArray;
@property(copy, nonatomic, readwrite) NSString *cellIdentifier;
@end

@implementation MallCollectionViewDataSource

- (instancetype)initDataSourceWithDataArray:(NSArray *)dataarray
                             cellIdentifier:(NSString *)identifier
                         cellConfigureBlock:
                             (MallCollectionViewCellConfigureBlock)block {
  self = [super init];
  if (self) {
    self.dataArray = dataarray;
    self.cellIdentifier = identifier;
    self.block = block;
  }
  return self;
}

#pragma mark - public method
- (void)refreshDataSourceWithData:(NSArray *)data {
  self.dataArray = data;
}

#pragma mark - private method
- (id)dataForRowAtIndexPath:(NSIndexPath *)indexPath {
  return self.dataArray[indexPath.item];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:
    (UICollectionView *)collectionView {
  return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewCell *cell =
      [collectionView dequeueReusableCellWithReuseIdentifier:self.cellIdentifier
                                                forIndexPath:indexPath];
  id cellData = [self dataForRowAtIndexPath:indexPath];
  self.block(cell, cellData);

  return cell;
}

- (NSArray *)dataArray {
  if (!_dataArray) {
    _dataArray = @[];
  }
  return _dataArray;
}

@end
