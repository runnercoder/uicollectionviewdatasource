//
//  MallCollectionViewDataSource.h
//  DCMall
//
//  Created by 吴恺 on 15/8/31.
//  Copyright (c) 2015年 wukai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^MallCollectionViewCellConfigureBlock)(id cell, id cellData);

@interface MallCollectionViewDataSource : NSObject<UICollectionViewDataSource>
@property(copy, nonatomic, readonly) MallCollectionViewCellConfigureBlock block;
@property(copy, nonatomic, readonly) NSArray *dataArray;
@property(copy, nonatomic, readonly) NSString *cellIdentifier;
- (instancetype)initDataSourceWithDataArray:(NSArray *)dataarray
                             cellIdentifier:(NSString *)identifier
                         cellConfigureBlock:
                             (MallCollectionViewCellConfigureBlock)block;
- (void)refreshDataSourceWithData:(NSArray *)data;
@end
