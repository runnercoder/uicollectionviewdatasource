# UICollectionDataSource

[![CI Status](http://img.shields.io/travis/starrykai/UICollectionDataSource.svg?style=flat)](https://travis-ci.org/starrykai/UICollectionDataSource)
[![Version](https://img.shields.io/cocoapods/v/UICollectionDataSource.svg?style=flat)](http://cocoapods.org/pods/UICollectionDataSource)
[![License](https://img.shields.io/cocoapods/l/UICollectionDataSource.svg?style=flat)](http://cocoapods.org/pods/UICollectionDataSource)
[![Platform](https://img.shields.io/cocoapods/p/UICollectionDataSource.svg?style=flat)](http://cocoapods.org/pods/UICollectionDataSource)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UICollectionDataSource is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "UICollectionDataSource"
```

## Author

starrykai, starryskykai@gmail.com

## License

UICollectionDataSource is available under the MIT license. See the LICENSE file for more info.
